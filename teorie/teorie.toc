\select@language {british}
\select@language {czech}
\contentsline {chapter}{\numberline {1}Indexace}{3}{chapter.1}
\contentsline {section}{\numberline {1.1}Postup indexace}{3}{section.1.1}
\contentsline {subsection}{\numberline {1.1.1}Tokenizace}{3}{subsection.1.1.1}
\contentsline {subsection}{\numberline {1.1.2}Odstran\IeC {\v e}n\IeC {\'\i } nev\IeC {\'y}znamov\IeC {\'y}ch slov}{4}{subsection.1.1.2}
\contentsline {subsection}{\numberline {1.1.3}Normalizace}{4}{subsection.1.1.3}
\contentsline {subsection}{\numberline {1.1.4}Invertovan\IeC {\'y} seznam}{5}{subsection.1.1.4}
\contentsline {chapter}{\numberline {2}Reprezentace textov\IeC {\'y}ch dokument\IeC {\r u}}{6}{chapter.2}
\contentsline {section}{\numberline {2.1}Bag of words}{6}{section.2.1}
\contentsline {section}{\numberline {2.2}Booleovsk\IeC {\'y} model}{6}{section.2.2}
\contentsline {section}{\numberline {2.3}Vektorov\IeC {\'y} model}{7}{section.2.3}
\contentsline {section}{\numberline {2.4}Latent Semantic Analysis}{8}{section.2.4}
\contentsline {section}{\numberline {2.5}Latent Dirichlet Allocation}{9}{section.2.5}
\contentsline {chapter}{\numberline {3}V\IeC {\'a}hov\IeC {\'a}n\IeC {\'\i } term\IeC {\r u}}{12}{chapter.3}
\contentsline {section}{\numberline {3.1}Bin\IeC {\'a}rn\IeC {\'\i } v\IeC {\'a}ha}{12}{section.3.1}
\contentsline {section}{\numberline {3.2}Frekvence termu}{12}{section.3.2}
\contentsline {section}{\numberline {3.3}Inverzn\IeC {\'\i } frekvence dokumentu}{13}{section.3.3}
\contentsline {section}{\numberline {3.4}tf-idf}{13}{section.3.4}
\contentsline {chapter}{\numberline {4}Porovn\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } dokument\IeC {\r u}}{15}{chapter.4}
\contentsline {section}{\numberline {4.1}Euklidovsk\IeC {\'a} vzd\IeC {\'a}lenost}{15}{section.4.1}
\contentsline {section}{\numberline {4.2}Kosinov\IeC {\'a} podobnost}{15}{section.4.2}
\contentsline {section}{\numberline {4.3}Jaccard\IeC {\r u}v index}{16}{section.4.3}
\contentsline {section}{\numberline {4.4}Kullback-Leibler divergence}{17}{section.4.4}
\contentsline {chapter}{\numberline {5}Vyhodnocen\IeC {\'\i } v\IeC {\'y}sledk\IeC {\r u}}{18}{chapter.5}
\contentsline {section}{\numberline {5.1}Precision}{18}{section.5.1}
\contentsline {section}{\numberline {5.2}\IeC {\'U}plnost}{18}{section.5.2}
\contentsline {section}{\numberline {5.3}Accuracy}{18}{section.5.3}
\contentsline {section}{\numberline {5.4}F-m\IeC {\'\i }ra}{19}{section.5.4}
\contentsline {chapter}{\numberline {6}Detekce t\IeC {\'e}m\IeC {\v e}\IeC {\v r} duplicitn\IeC {\'\i }ch dokument\IeC {\r u}}{20}{chapter.6}
\contentsline {section}{\numberline {6.1}Shingling}{20}{section.6.1}
\contentsline {section}{\numberline {6.2}MinHashing}{21}{section.6.2}
\contentsline {section}{\numberline {6.3}Locality Sensitive Hashing}{22}{section.6.3}
\contentsline {chapter}{\numberline {7}Rozpozn\IeC {\'a}v\IeC {\'a}n\IeC {\'\i } pojmenovan\IeC {\'y}ch entit}{23}{chapter.7}
\contentsline {section}{\numberline {7.1}N\IeC {\'a}stroje pro anglick\IeC {\'y} jazyk}{23}{section.7.1}
\contentsline {section}{\numberline {7.2}N\IeC {\'a}stroj pro \IeC {\v c}esk\IeC {\'y} jazyk}{24}{section.7.2}
\contentsline {chapter}{\numberline {8}Apache Lucene}{27}{chapter.8}
\contentsline {section}{\numberline {8.1}Podobnost v Lucene}{27}{section.8.1}
\contentsline {section}{\numberline {8.2}Lucene scoring formula}{28}{section.8.2}
\contentsline {section}{\numberline {8.3}More like this}{29}{section.8.3}
\contentsline {section}{\numberline {8.4}Apache Tika}{29}{section.8.4}
\contentsline {section}{\numberline {8.5}Luke}{29}{section.8.5}
\contentsline {chapter}{Literatura}{31}{chapter*.2}
