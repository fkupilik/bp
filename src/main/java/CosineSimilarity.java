import java.io.IOException;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.math3.linear.RealVector;
import org.apache.lucene.document.Document;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.Term;
import org.apache.lucene.index.Terms;
import org.apache.lucene.index.TermsEnum;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.lucene.util.BytesRef;

public class CosineSimilarity {
	
	RealVector v1;
	RealVector v2;
	
	IndexReader reader;
	
	private Map<String, Integer> vsechnyTermy;
	
	DocVector[] docVector;
	
	public CosineSimilarity(String indexDirectory) throws IOException{
		Directory directory = FSDirectory.open(Paths.get(indexDirectory));
		reader = DirectoryReader.open(directory);
		
        Map<String, Integer> docFrequencies = new HashMap<>();
        Map<String, Integer> termFrequencies = new HashMap<>();
        Map<String, Double> tf_Idf_Weights = new HashMap<>();
        
        vsechnyTermy = new HashMap<>();
        
        int pom = 0;
        
        for(int i = 0; i < reader.maxDoc(); i++){
        	try {
        		Terms vector = reader.getTermVector(i, "text");
        		TermsEnum termsEnum = null;
        		termsEnum = vector.iterator();
        		
        		BytesRef text = null;
        		while ((text = termsEnum.next()) != null) {
        			String term = text.utf8ToString();
        			vsechnyTermy.put(term, pom++);
        			
        		}				
			} catch (Exception e) {
				System.out.println("Neco je null");
			}
        }
        
        
        pom = 0;
        for(Entry<String,Integer> s : vsechnyTermy.entrySet())
        {        
//            System.out.println(s.getKey());
            s.setValue(pom++);
        }
        
        System.out.println(vsechnyTermy.size());
	}
	
	public Map<String, Double> vratVahy(IndexReader reader, int docID) throws IOException{
		Terms vector = reader.getTermVector(docID, "text");
		TermsEnum termsEnum = null;
		termsEnum = vector.iterator();
		
        Map<String, Integer> docFrequencies = new HashMap<>();
        Map<String, Integer> termFrequencies = new HashMap<>();
        Map<String, Double> tf_Idf_Weights = new HashMap<>();
        
        BytesRef text = null;
        
		while ((text = termsEnum.next()) != null) {
            String term = text.utf8ToString();
            int docFreq = termsEnum.docFreq();
            docFrequencies.put(term, reader.docFreq( new Term( "text", term ) ));
		}
        
	}
	
/*	public DocVector[] getDocumentVectors(){
		docVector = new DocVector[reader.maxDoc()];
		for(int i = 0; i < reader.maxDoc(); i++){
            Terms vector = reader.getTermVector(i, "text");
            TermsEnum termsEnum = null;
            termsEnum = vector.iterator();
            BytesRef text = null;            
            docVector[i] = new DocVector(vsechnyTermy);            
            while ((text = termsEnum.next()) != null) {
                String term = text.utf8ToString();
                int freq = (int) termsEnum.totalTermFreq();
                docVector[i].setEntry(term, freq);
            }
            docVector[i].normalize();
		}
	}*/

}
