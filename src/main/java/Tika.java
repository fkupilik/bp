
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.ContentHandler;
import java.nio.file.FileSystemException;
import java.nio.file.Path;
import java.nio.file.Paths;

import org.apache.commons.lang.StringUtils;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.codecs.lucene50.Lucene50CompoundFormat;
import org.apache.lucene.document.Document;
import org.apache.lucene.document.Field;
import org.apache.lucene.document.Field.Store;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.document.StringField;
import org.apache.lucene.document.TextField;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.queryparser.classic.ParseException;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.search.IndexSearcher;
import org.apache.lucene.search.Query;
import org.apache.lucene.search.ScoreDoc;
import org.apache.lucene.search.TopDocs;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.FSDirectory;
import org.apache.tika.exception.TikaException;
import org.apache.tika.metadata.Metadata;
import org.apache.tika.parser.AutoDetectParser;
import org.apache.tika.parser.ParseContext;
import org.apache.tika.parser.Parser;
import org.apache.tika.sax.BodyContentHandler;
import org.xml.sax.SAXException;




public class Tika {

	public String indexDirectory = "index";
	public String documents = "documents";
	
	public String documentsNove = "/home/filip/Dokumenty/Skola/bakalarka/dataKonopikSpravna/documents/doc";
	public String indexNove = "indexNove";
	
	public static final FieldType TYPE_STORED = new FieldType();
	
    static {
    	TYPE_STORED.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
        TYPE_STORED.setTokenized(true);
        TYPE_STORED.setStored(true);
        TYPE_STORED.setStoreTermVectors(true);
        TYPE_STORED.setStoreTermVectorPositions(true);
        TYPE_STORED.freeze();
    }


	public void writeIndex(String indexDirectory, String sourceDirectory) throws IOException{
		
		File docs = new File(sourceDirectory);
		
		Directory directory = FSDirectory.open(Paths.get(indexDirectory));
		StandardAnalyzer analyzer = new StandardAnalyzer();
		IndexWriterConfig config = new IndexWriterConfig(analyzer);
		IndexWriter writer = new IndexWriter(directory, config);
		
		writer.deleteAll();
		
		for (File file : docs.listFiles()) {
			Metadata metadata = new Metadata();
			BodyContentHandler handler = new BodyContentHandler(-1);
			ParseContext context = new ParseContext();
			Parser parser = new AutoDetectParser();
			InputStream stream = new FileInputStream(file);
			
			try {
				parser.parse(stream, handler, metadata, context);
			} catch (SAXException | TikaException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}finally{
				stream.close();
			}
			
//			String content = handler.toString();
			String fileName = file.getName();
//			System.out.println(fileName);
			
			Document doc = new Document();
			StringField filename = new StringField("fileName", fileName, Store.YES);

			Field content = new Field("text", handler.toString(), TYPE_STORED);
			
			doc.add(filename);
			doc.add(content);
			
			for (String key : metadata.names()) {
				String name = key.toLowerCase();
				String value = metadata.get(key);
				
				if(StringUtils.isBlank(value)){
					continue;
				}
				doc.add(new StringField(name, value, Store.YES));
			}
			writer.addDocument(doc);
		}
		
		writer.commit();
		writer.deleteUnusedFiles();
		
		System.out.println(writer.maxDoc() + " documents written");
		writer.close();
	}
	
	
	public void searchIndex(String indexDirectory) throws IOException{
		
		Directory directory = FSDirectory.open(Paths.get(indexDirectory));
		
		Query query = null;
		try {
			query = new QueryParser("text", new StandardAnalyzer()).parse("greenhouse");
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		IndexReader reader = DirectoryReader.open(directory);
		IndexSearcher searcher = new IndexSearcher(reader);
		
		TopDocs hits = searcher.search(query, 1000);
		
		System.out.println("Nalezeno " + hits.totalHits + " dokumentu");
		for (ScoreDoc scoreDoc : hits.scoreDocs) {
			Document doc = reader.document(scoreDoc.doc);
			System.out.println(doc.get("fileName"));
		}
	}
	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
		Tika test = new Tika();
//		test.writeIndex(test.indexNove, test.documentsNove);
//		test.searchIndex(test.indexDirectory);
		CosineSimilarity cosine = new CosineSimilarity(test.indexNove);
		
	}

}
